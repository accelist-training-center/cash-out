﻿using CashOut.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CashOut.Data
{
    /// <summary>
    /// A static class for handling static registered product data.
    /// Dont' randomly use the "static" keyword!
    /// This is just for a simple example.
    /// Reference: https://stackoverflow.com/questions/1315086/should-you-avoid-static-classes.
    /// </summary>
    public static class ProductData
    {
        /// <summary>
        /// A static object will always linger in your RAM while the application is still running.
        /// A static object to store list of the registered product.
        /// </summary>
        public static List<ProductModel> Products { get; set; } = new List<ProductModel> 
        {
            new ProductModel
            {
                ProductCode = "NS",
                Name = "Nintendo Switch",
                Price = 3_500_000,
                Stock = 10
            },
            new ProductModel
            {
                ProductCode = "PS4",
                Name = "PS4 Black",
                Price = 4_000_000,
                Stock = 5
            }
        };

        /// <summary>
        /// Get the <seealso cref="ProductModel"/> object from the registered product list.
        /// </summary>
        /// <param name="productCode">The requested Product Code.</param>
        /// <returns>Return the <seealso cref="ProductModel"/> object.</returns>
        public static ProductModel GetRegisteredProduct(string productCode)
        {
            foreach (var product in ProductData.Products)
            {
                if (productCode == product.ProductCode)
                {
                    return product;
                }
            }

            // Return null value if not found.
            return null;
        }

        /// <summary>
        /// Validate whether the productCode parameter has been registered in the registreted product list.
        /// This method running time is O(N). For the best performance, a process must be running at O(1).
        /// Reference: https://rob-bell.net/2009/06/a-beginners-guide-to-big-o-notation.
        /// </summary>
        /// <returns>True if exists, otherwise return false.</returns>
        public static bool ValidateExistingProductCode(string productCode)
        {
            foreach (var product in ProductData.Products)
            {
                if (productCode == product.ProductCode)
                {
                    return true;
                }
            }

            return false;

            // You can use Exists.
            //return ProductData.Products
            //    .Exists(Q => Q.ProductCode == productCode);

            // Or you use the LINQ way.
            //var existingProductCode = ProductData.Products
            //    .Where(Q => Q.ProductCode == productCode)
            //    .Select(Q => Q.ProductCode)
            //    .FirstOrDefault();
        }
    }
}
