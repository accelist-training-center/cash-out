﻿using CashOut.Data;
using CashOut.Interfaces;
using CashOut.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CashOut.Menus
{
    /// <summary>
    /// This is a XML comment.
    /// XML comment is a must, if you want to ensure your source codes is well documented. Try hover "ProductMenu" to see it in action.
    /// This is a class that server various implementations for providing Product menus.
    /// </summary>
    public class ProductMenu : IMenu
    {
        /// <summary>
        /// <seealso cref="List{T}"/> is a specialized data type to store a number of items into one collection.
        /// You can consider this as an array, but without limit (except your RAM size).
        /// Reference: https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.list-1?view=netcore-3.1.
        /// A "readonly" keyword is used to prevent variable assignment outside of constructor or its variable declaration.
        /// Reference: https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/readonly.
        /// Don't forget to assign an object (that is not using a primitive data type) to avoid NullReferenceException.
        /// </summary>
        public readonly List<string> Menus = new List<string>();

        /// <summary>
        /// Whenever you see a getter and setter, then it is called a property.
        /// Reference: https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/properties.
        /// This property is used to store the initial menu text.
        /// </summary>
        public string InitialMenuText { get; set; } = "Product List menus:";

        /// <summary>
        /// Constructor for this class.
        /// Members declaration such as property and constructor will always run whenever a class is initialized.
        /// Reference: https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/operators/new-operator.
        /// </summary>
        public ProductMenu()
        {
            // With List data type, you can call "Add()" method for easier item insertion into the collection.
            this.Menus.Add("1. Add Product");
            this.Menus.Add("2. Update Product");
            this.Menus.Add("3. Delete Product");
            this.Menus.Add("4. Back");
        }

        /// <summary>
        /// Display the product management menus.
        /// </summary>
        public void DisplayMenus()
        {
            var isExit = false;
            while (isExit == false)
            {
                // Will clear the console output.
                Console.Clear();

                Console.WriteLine("Product List:");
                this.DisplayRegisteredProducts();

                this.DisplayMenuSelection();

                var isInt = int.TryParse(Console.ReadLine(), out var inputNumber);

                // Check if the input is an integer or not.
                if (isInt == false)
                {
                    Console.WriteLine("Please input a valid number");
                }
                else
                {
                    if (inputNumber != 4)
                    {
                        switch (inputNumber)
                        {
                            case 1:
                                this.AddNewProduct();
                                break;
                            case 2:
                                this.UpdateProduct();
                                break;
                            case 3:
                                this.DeleteProduct();
                                break;
                            default:
                                Console.WriteLine("Please enter a valid menu");
                                break;
                        }
                    }
                    else
                    {
                        isExit = true;
                    }
                }
                Console.Clear();
            };
        }

        /// <summary>
        /// Display the menu selection text.
        /// </summary>
        public void DisplayMenuSelection()
        {
            Console.WriteLine(this.InitialMenuText);

            // Remember to use "foreach" first!.
            // If not compatible with your iteration mechanisms, at least prefer "for" to avoid unlimited loop potential!
            // Reference: https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/foreach-in.
            foreach (var menu in this.Menus)
            {
                Console.WriteLine(menu);
            }

            Console.WriteLine("Please input your choice:");
        }

        /// <summary>
        /// Validate the product code input.
        /// </summary>
        /// <returns></returns>
        private string ValidateProductCode()
        {
            var isValidProductCode = false;
            var productCode = string.Empty;

            // Validate the Product Code input.
            var productCodeInputMessage = "Please enter your Product Code:";
            Console.WriteLine(productCodeInputMessage);
            while (isValidProductCode == false)
            {
                var productCodeInput = Console.ReadLine();

                if (productCodeInput.Length < 2)
                {
                    Console.WriteLine("The Product Code length must be 2 or greater");
                    Console.WriteLine(productCodeInputMessage);
                }
                else
                {
                    var isProductCodeExists = ProductData.ValidateExistingProductCode(productCodeInput);

                    if (isProductCodeExists == true)
                    {
                        Console.WriteLine("The same Product Code was found. Please use another Product Code.");
                        Console.WriteLine(productCodeInputMessage);
                    }
                    else
                    {
                        productCode = productCodeInput;
                        isValidProductCode = true;
                    }
                }
            }

            return productCode;
        }

        /// <summary>
        /// Validate the product name input.
        /// </summary>
        /// <returns></returns>
        private string ValidateProductName()
        {
            var isValidName = false;
            var name = "";

            // Validate the Product Name input.
            var productNameInputMessage = "Please enter your Product Name:";
            Console.WriteLine(productNameInputMessage);
            while (isValidName == false)
            {
                var nameInput = Console.ReadLine();

                if (nameInput.Length < 2)
                {
                    Console.WriteLine("The Product Name length must be 2 or greater");
                    Console.WriteLine(productNameInputMessage);
                }
                else
                {
                    name = nameInput;
                    isValidName = true;
                }
            }

            return name;
        }

        /// <summary>
        /// Validate the product stock input.
        /// </summary>
        /// <returns></returns>
        private int ValidateProductStock()
        {
            var isStockValid = false;
            var stock = 0;

            // Validate the Product Stock input.
            var productStockInputMessage = "Please enter your Product Stock:";
            Console.WriteLine(productStockInputMessage);
            while (isStockValid == false)
            {
                var isStockInt = int.TryParse(Console.ReadLine(), out var inputStock);

                if (isStockInt == false)
                {
                    Console.WriteLine("Please input a valid stock number");
                    Console.WriteLine(productStockInputMessage);
                }
                else
                {
                    if (inputStock < 0)
                    {
                        Console.WriteLine("The stock number cannot be less than 0");
                        Console.WriteLine(productStockInputMessage);
                    }
                    else
                    {
                        stock = inputStock;
                        isStockValid = true;
                    }
                }
            }

            return stock;
        }

        /// <summary>
        /// Validate the product price input.
        /// </summary>
        /// <returns></returns>
        private decimal ValidateProductPrice()
        {
            var isPriceValid = false;

            // M is suffix for defining the assigned value is a decimal data type.
            // Reference: https://www.c-sharpcorner.com/article/data-type-suffixes-in-c-sharp/.
            var price = 0M;

            // Validate the Product Price input.
            var productPriceInputMessage = "Please enter your Product Price:";
            Console.WriteLine(productPriceInputMessage);
            while (isPriceValid == false)
            {
                var isPriceDecimal = decimal.TryParse(Console.ReadLine(), out var inputPrice);

                if (isPriceDecimal == false)
                {
                    Console.WriteLine("Please input a valid price number");
                    Console.WriteLine(productPriceInputMessage);
                }
                else
                {
                    if (inputPrice <= 100)
                    {
                        Console.WriteLine("The Product Price cannot be less than 100");
                        Console.WriteLine(productPriceInputMessage);
                    }
                    else
                    {
                        price = inputPrice;
                        isPriceValid = true;
                    }
                }
            }

            return price;
        }

        /// <summary>
        /// Add new product to the registered product list.
        /// </summary>
        public void AddNewProduct()
        {
            var productCode = this.ValidateProductCode();

            var productName = this.ValidateProductName();

            var productStock = this.ValidateProductStock();

            var productPrice = this.ValidateProductPrice();

            // Add the product data into the static list.
            ProductData.Products.Add(new ProductModel
            {
                ProductCode = productCode,
                Name = productName,
                // You can separate the number notation like this for easier reading.
                // Price = 3_000_000,
                Price = productPrice,
                Stock = productStock
            });
        }

        /// <summary>
        /// Display the registered product list.
        /// </summary>
        public void DisplayRegisteredProducts()
        {
            Console.WriteLine("| Product Code | Name | Price | Stock |");
            foreach (var product in ProductData.Products)
            {
                // $ is a symbol for string interpolation
                // Reference: https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/tokens/interpolated.
                Console.WriteLine($"| {product.ProductCode} | {product.Name} | {product.Price} | {product.Stock} |");
            }
        }

        /// <summary>
        /// Update the existing product.
        /// </summary>
        public void UpdateProduct()
        {
            var isValidProductCode = false;
            var productCode = "";

            Console.WriteLine("Please enter your Product Code (type exit to return):");
            while (isValidProductCode == false)
            {
                var productCodeInput = Console.ReadLine();

                if (productCodeInput == "exit")
                {
                    return;
                }

                var isProductCodeExists = ProductData.ValidateExistingProductCode(productCodeInput);

                if (isProductCodeExists == false)
                {
                    Console.WriteLine("The Product Code was not found. Please re-enter your Product Code.");
                }
                else
                {
                    productCode = productCodeInput;
                    isValidProductCode = true;
                }
            }

            // Because this object was obtained from a List of ProductModel (both List and ProductModel are reference type), all changes to the members of this object will automatically referenced to the original object.
            // Reference: https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/reference-types.
            // Another Reference: https://www.tutorialsteacher.com/csharp/csharp-value-type-and-reference-type.
            var existingProduct = ProductData.GetRegisteredProduct(productCode);

            var isValidName = false;

            Console.WriteLine("Please enter your Product Name:");
            while (isValidName == false)
            {
                var nameInput = Console.ReadLine();

                if (nameInput.Length < 2)
                {
                    Console.WriteLine("The Product Name length must be 2 or greater");
                }
                else
                {
                    existingProduct.Name = nameInput;
                    isValidName = true;
                }
            }

            var isStockValid = false;

            Console.WriteLine("Please enter your Product Stock:");
            while (isStockValid == false)
            {
                var isStockInt = int.TryParse(Console.ReadLine(), out var inputStock);

                if (isStockInt == false)
                {
                    Console.WriteLine("Please input a valid stock number");
                }
                else
                {
                    existingProduct.Stock = inputStock;
                    isStockValid = true;
                }
            }

            var isPriceValid = false;

            Console.WriteLine("Please enter your Product Price:");
            while (isPriceValid == false)
            {
                var isPriceDecimal = decimal.TryParse(Console.ReadLine(), out var inputPrice);

                if (isPriceDecimal == false)
                {
                    Console.WriteLine("Please input a valid price number");
                }
                else
                {
                    if (inputPrice <= 100)
                    {
                        Console.WriteLine("The Product Price must be greater than 100");
                    }
                    else
                    {
                        existingProduct.Price = inputPrice;
                        isPriceValid = true;
                    }
                }
            }
        }

        /// <summary>
        /// Delete the existing product.
        /// </summary>
        public void DeleteProduct()
        {
            var isValidProductCode = false;
            var productCode = "";

            Console.WriteLine("Please enter your Product Code (type exit to return):");
            while (isValidProductCode == false)
            {
                var productCodeInput = Console.ReadLine();

                if (productCodeInput == "exit")
                {
                    return;
                }

                var isProductCodeExists = ProductData.ValidateExistingProductCode(productCodeInput);

                if (isProductCodeExists == false)
                {
                    Console.WriteLine("The Product Code was not found. Please re-enter your Product Code.");
                }
                else
                {
                    productCode = productCodeInput;
                    isValidProductCode = true;
                }
            }

            // Check the UpdateProduct() method for more information.
            var existingProduct = ProductData.GetRegisteredProduct(productCode);

            /* 
             * Because existingProduct object is a reference type, 
             * the Remove() extension method can be used by passing the existingProduct object in its parameter
             * Reference: https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.list-1.remove?view=netcore-3.1.
             */
            ProductData.Products.Remove(existingProduct);

            /* 
             * Normally, you only need the primary key of the object structure, in this case, ProductCode,
             * which will be used to delete the existing data in the SQL database by using the ProductCode.
             */
        }
    }
}
