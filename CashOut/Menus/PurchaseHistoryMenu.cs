﻿using CashOut.Data;
using CashOut.Interfaces;
using System;
using System.Collections.Generic;

namespace CashOut.Menus
{
    public class PurchaseHistoryMenu : IMenu
    {
        public readonly List<string> Menus = new List<string>
        {
            "1. Back"
        };

        public void DisplayMenus()
        {
            Console.Clear();
            Console.WriteLine("View Purchase History:");
            Console.WriteLine("| Product Code | Name | Price | Quantity | Sub Total |");

            var histories = PurchaseHistoryData.Histories;

            foreach (var history in histories)
            {
                Console.WriteLine($"| {history.ProductCode} | {history.ProductName} | {history.Price} | {history.Quantity} | {history.SubTotal} |");
            }

            Console.WriteLine($"Total: {PurchaseHistoryData.Total}");

            this.DisplayMenuSelection();

            var isExit = false;
            while (isExit == false)
            {
                var isInt = int.TryParse(Console.ReadLine(), out var inputNumber);

                // Check if the input is an integer or not.
                if (isInt == false)
                {
                    Console.WriteLine("Please input a valid number");
                }
                else
                {
                    if (inputNumber != 1)
                    {
                        Console.WriteLine("Please enter a valid menu");
                    }
                    else
                    {
                        isExit = true;
                    }
                }
            }
            
        }

        /// <summary>
        /// Display the menu selection text.
        /// </summary>
        public void DisplayMenuSelection()
        {
            Console.WriteLine("Menu:");

            // Remember to use "foreach" first!.
            // If not compatible with your iteration mechanisms, at least prefer "for" to avoid unlimited loop potential!
            // Reference: https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/foreach-in.
            foreach (var menu in this.Menus)
            {
                Console.WriteLine(menu);
            }

            Console.WriteLine("Please input your choice:");
        }
    }
}
