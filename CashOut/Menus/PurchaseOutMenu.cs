﻿using CashOut.Data;
using CashOut.Interfaces;
using CashOut.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CashOut.Menus
{
    public class PurchaseOutMenu : IMenu
    {
        public readonly List<string> Menus = new List<string> 
        {
            "1. Add product to cart",
            "2. Remove product from cart",
            "3. Update product quantity in cart",
            "4. Checkout",
            "5. Back"
        };

        /// <summary>
        /// Display the purchase out menus.
        /// </summary>
        public void DisplayMenus()
        {
            var isExit = false;
            while (isExit == false)
            {
                // Will clear the console output.
                Console.Clear();

                Console.WriteLine("Purchase Out");
                this.DisplayRegisteredProducts();

                Console.WriteLine();

                this.DisplayUserCart();

                Console.WriteLine();

                this.DisplayMenuSelection();

                var isInt = int.TryParse(Console.ReadLine(), out var inputNumber);

                // Check if the input is an integer or not.
                if (isInt == false)
                {
                    Console.WriteLine("Please input a valid number");
                }
                else
                {
                    if (inputNumber != 5)
                    {
                        switch (inputNumber)
                        {
                            case 1:
                                this.AddItemToCart();
                                break;
                            case 2:
                                this.RemoveItemFromCart();
                                break;
                            case 3:
                                this.UpdateItemQuantity();
                                break;
                            case 4:
                                Console.Clear();
                                this.Checkout();
                                break;
                            default:
                                Console.WriteLine("Please enter a valid menu");
                                break;
                        }
                    }
                    else
                    {
                        isExit = true;
                    }
                }
                Console.Clear();
            };
        }

        /// <summary>
        /// Display the registered product list.
        /// </summary>
        public void DisplayRegisteredProducts()
        {
            Console.WriteLine("Product List:");
            Console.WriteLine("| Product Code | Name | Price | Stock |");
            foreach (var product in ProductData.Products)
            {
                Console.WriteLine($"| {product.ProductCode} | {product.Name} | {product.Price} | {product.Stock} |");
            }
        }

        /// <summary>
        /// Display the user cart items.
        /// </summary>
        public void DisplayUserCart()
        {
            Console.WriteLine("User Cart:");
            Console.WriteLine("| Product Code | Name | Price | Quantity |");

            var cartItems = this.GetCartItems();

            foreach (var item in cartItems)
            {
                Console.WriteLine($"| {item.ProductCode} | {item.Name} | {item.Price} | {item.Quantity} |");
            }
        }

        /// <summary>
        /// Get list of JOINED Product and Cart Item data.
        /// </summary>
        /// <returns>Return <seealso cref="List{T}"/> of <seealso cref="CartModel"/> object.</returns>
        public List<CartModel> GetCartItems()
        {
            /*
             * the "query" variable hasn't executed yet, so you won't get list of CartModel object yet.
             * You can read this in SQL query format as:
             * SELECT p.ProductCode,
             *        p.Name,
             *        p.Price,
             *        ci.Quantity
             * FROM CartItems ci
             *      JOIN Products p ON ci.ProductCode = p.ProductCode
             */
            var query = from ci in CartData.CartItems
                        join p in ProductData.Products on ci.ProductCode equals p.ProductCode
                        select new CartModel
                        {
                            ProductCode = p.ProductCode,
                            Name = p.Name,
                            Price = p.Price,
                            Quantity = ci.Quantity
                        };

            // Query and execute the "query" variable to obtain list of CartModel object.
            var cartItems = query.ToList();

            return cartItems;
        }

        /// <summary>
        /// Display the menu selection text.
        /// </summary>
        public void DisplayMenuSelection()
        {
            Console.WriteLine("Menus:");

            // Remember to use "foreach" first!.
            // If not compatible with your iteration mechanisms, at least prefer "for" to avoid unlimited loop potential!
            // Reference: https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/foreach-in.
            foreach (var menu in this.Menus)
            {
                Console.WriteLine(menu);
            }

            Console.WriteLine("Please input your choice:");
        }

        /// <summary>
        /// Add a new item into user cart.
        /// </summary>
        public void AddItemToCart()
        {
            var isValidProductCode = false;
            var productCode = "";

            Console.WriteLine("Please enter your Product Code (type exit to return):");
            while (isValidProductCode == false)
            {
                var productCodeInput = Console.ReadLine();

                if (productCodeInput == "exit")
                {
                    return;
                }

                var isProductCodeExists = ProductData.ValidateExistingProductCode(productCodeInput);

                if (isProductCodeExists == false)
                {
                    Console.WriteLine("The Product Code was not found. Please re-enter your Product Code.");
                }
                else
                {
                    productCode = productCodeInput;
                    isValidProductCode = true;
                }
            }

            var existingProduct = ProductData.GetRegisteredProduct(productCode);

            if (existingProduct.Stock < 1)
            {
                Console.WriteLine("We are sorry, but the product is out of stock. Press any key to continue...");
                Console.ReadKey();
                return;
            }

            var existingCartItem = CartData.GetCartItem(existingProduct.ProductCode);

            if (existingCartItem != null && existingProduct.Stock <= existingCartItem.Quantity)
            {
                Console.WriteLine("Your current cart item for this product has reached its stock limit. Press any key to continue...");
                Console.ReadKey();
                return;
            }

            var isQuantity = false;
            var quantity = 0;
            
            Console.WriteLine("Please enter your item quantity:");
            while (isQuantity == false)
            {
                var isQuantityInt = int.TryParse(Console.ReadLine(), out var inputQuantity);

                if (isQuantityInt == false)
                {
                    Console.WriteLine("Please input a valid stock number");
                }
                else
                {
                    // Validate quantity must be greater than 0;
                    if (inputQuantity < 1)
                    {
                        Console.WriteLine("The item quantity must be greater than 0.");
                    }
                    else if (inputQuantity > existingProduct.Stock)
                    {
                        Console.WriteLine("The item quantity must be less or equal than the existing product stock.");
                    }
                    else if (existingCartItem != null && (existingCartItem.Quantity + inputQuantity) > existingProduct.Stock)
                    {
                        Console.WriteLine("The combined item quantity in your cart with your new input must be less or equal than the existing product stock.");
                    }
                    else
                    {
                        quantity = inputQuantity;
                        isQuantity = true;
                    }
                }
            }

            // If not null, then the item/object was found in user cart.
            if (existingCartItem != null)
            {
                existingCartItem.Quantity += quantity;
            }
            else
            {
                CartData.CartItems.Add(new CartItemModel
                {
                    ProductCode = existingProduct.ProductCode,
                    Quantity = quantity
                });
            }
        }

        /// <summary>
        /// Remove existing item from user cart.
        /// </summary>
        public void RemoveItemFromCart()
        {
            var isValidProductCode = false;
            var existingCartItem = new CartItemModel();

            Console.WriteLine("Please enter your Product Code (type exit to return):");
            while (isValidProductCode == false)
            {
                var productCodeInput = Console.ReadLine();

                if (productCodeInput == "exit")
                {
                    return;
                }

                existingCartItem = CartData.GetCartItem(productCodeInput);

                if (existingCartItem == null)
                {
                    Console.WriteLine("The Product Code was not found. Please re-enter your Product Code.");
                }
                else
                {
                    isValidProductCode = true;
                }
            }

            CartData.CartItems.Remove(existingCartItem);
        }

        public void UpdateItemQuantity()
        {
            var isValidProductCode = false;
            var productCode = "";

            var existingCartItem = new CartItemModel();

            Console.WriteLine("Please enter your Product Code (type exit to return):");
            while (isValidProductCode == false)
            {
                var productCodeInput = Console.ReadLine();

                if (productCodeInput == "exit")
                {
                    return;
                }

                existingCartItem = CartData.GetCartItem(productCodeInput);

                if (existingCartItem == null)
                {
                    Console.WriteLine("The Product Code was not found. Please re-enter your Product Code.");
                }
                else
                {
                    productCode = productCodeInput;
                    isValidProductCode = true;
                }
            }

            var existingProduct = ProductData.GetRegisteredProduct(productCode);

            if (existingProduct.Stock < 1)
            {
                Console.WriteLine("We are sorry, but the product is out of stock. Press any key to continue...");
                Console.ReadKey();
                return;
            }

            if (existingCartItem != null && existingProduct.Stock <= existingCartItem.Quantity)
            {
                Console.WriteLine("Your current cart item for this product has reached its stock limit. Press any key to continue...");
                Console.ReadKey();
                return;
            }

            var isQuantity = false;
            var quantity = 0;

            Console.WriteLine("Please enter your item quantity:");
            while (isQuantity == false)
            {
                var isQuantityInt = int.TryParse(Console.ReadLine(), out var inputQuantity);

                if (isQuantityInt == false)
                {
                    Console.WriteLine("Please input a valid stock number");
                }
                else
                {
                    // Validate quantity must be greater than 0;
                    if (inputQuantity < 1)
                    {
                        Console.WriteLine("The item quantity must be greater than 0.");
                    }
                    else if (inputQuantity > existingProduct.Stock)
                    {
                        Console.WriteLine("The item quantity must be less or equal than the existing product stock.");
                    }
                    else if (existingCartItem != null && (existingCartItem.Quantity + inputQuantity) > existingProduct.Stock)
                    {
                        Console.WriteLine("The combined item quantity in your cart with your new input must be less or equal than the existing product stock.");
                    }
                    else
                    {
                        quantity = inputQuantity;
                        isQuantity = true;
                    }
                }
            }

            existingCartItem.Quantity += quantity;
        }

        /// <summary>
        /// Proceed the checkout transaction.
        /// Will reduce the the product stock and clear user cart if user comply with the checkout process.
        /// </summary>
        public void Checkout()
        {
            this.DisplayUserCart();

            var totalPrice = 0M;

            foreach (var item in this.GetCartItems())
            {
                // += operator reference: https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/operators/addition-operator.
                totalPrice += item.Price * item.Quantity;
            }

            Console.WriteLine($"Total Price: {totalPrice}");
            Console.WriteLine("Proceed? (Type OK to proceed, otherwise type anything beside it to back)");

            var proceedInput = Console.ReadLine();

            if (proceedInput == "OK")
            {
                foreach (var product in ProductData.Products)
                {
                    var purchaseHistory = new PurchaseHistoryModel();
                    foreach (var item in CartData.CartItems)
                    {
                        if (product.ProductCode == item.ProductCode)
                        {
                            purchaseHistory.ProductCode = product.ProductCode;
                            purchaseHistory.ProductName = product.Name;
                            purchaseHistory.Price = product.Price;
                            purchaseHistory.Quantity = item.Quantity;
                            purchaseHistory.SubTotal = purchaseHistory.Quantity * purchaseHistory.Price;

                            PurchaseHistoryData.Histories.Add(purchaseHistory);
                            PurchaseHistoryData.Total = PurchaseHistoryData.Histories.Sum(Q => Q.SubTotal);

                            // -= operator reference: https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/operators/subtraction-operator.
                            product.Stock -= item.Quantity;
                        }
                    }
                }

                CartData.CartItems = new List<CartItemModel>();
            }
        }
    }
}
