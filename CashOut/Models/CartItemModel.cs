﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashOut.Models
{
    /// <summary>
    /// A model class for storing user cart data.
    /// Only contains the Product Code and its quantity in the user cart.
    /// </summary>
    public class CartItemModel
    {
        public string ProductCode { get; set; }

        public int Quantity { get; set; }
    }
}
