﻿using CashOut.Menus;
using System;

namespace CashOut
{
    class Program
    {
        static void Main(string[] args)
        {
            // You can only call a static method inside a static method.
            DisplayMenus();

            /*
             * Create a variable named "isExit" using "var" keyword: https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/var.
             * You can also use specific data type keyword, such as int or string, but "var" is simplier.
             */

            var isExit = false;
            do
            {
                /*
                 * int.TryParse is the default string-to-int parser method/function, which can be used to validate if a string can be parsed into an integer.
                 * The inputNumber will store the parsed int value.
                 */
                var isInt = int.TryParse(Console.ReadLine(), out var inputNumber);

                // Check if the input is an integer or not.
                if (isInt == false)
                {
                    Console.WriteLine("Please input a valid number");
                }
                else
                {
                    // Avoid arrow codes if possible: https://blog.codinghorror.com/flattening-arrow-code/.
                    // Check if the input value is not 3.
                    if (inputNumber != 4)
                    {
                        // Always use switch case for case like this, which have a better performance operation (O(1)).
                        // Reference: https://stackoverflow.com/questions/395618/is-there-any-significant-difference-between-using-if-else-and-switch-case-in-c.
                        switch (inputNumber)
                        {
                            case 1:
                                var productMenu = new ProductMenu();
                                productMenu.DisplayMenus();

                                Console.Clear();
                                DisplayMenus();
                                break;
                            case 2:
                                var purchaseOutMenu = new PurchaseOutMenu();
                                purchaseOutMenu.DisplayMenus();

                                Console.Clear();
                                DisplayMenus();
                                break;
                            case 3:
                                var purchaseHistoryMenu = new PurchaseHistoryMenu();
                                purchaseHistoryMenu.DisplayMenus();

                                Console.Clear();
                                DisplayMenus();
                                break;
                            default:
                                Console.WriteLine("Please enter a valid menu");
                                break;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Thank you for using Cash-Out.");
                        isExit = true;
                    }
                }
            } while (isExit == false);
        }

        static void DisplayMenus()
        {
            // Output the text to the console window.
            // The @ symbol is called verbatim string: https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/tokens/verbatim.
            // It allows you to write a multiline string, for example.
            Console.WriteLine(@"Welcome to Cash-Out.
Menus:
1. Product List
2. Purchase Out
3. View Purchase History
4. Exit

Your choice:");
        }
    }
}
